package homework8;

import homework8.DAO.CollectionFamilyDao;
import homework8.DAO.FamilyController;
import homework8.DAO.FamilyDao;
import homework8.DAO.FamilyService;
import homework8.Family.Dog;
import homework8.Family.Family;
import homework8.Family.Human;
import homework8.Family.RoboCat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Family> familyList = new ArrayList<>(List.of(
                new Family(new Human("pap1","gag1",1999),new Human("mam1","gag1",1999)),
                new Family(new Human("pap2","gag2",1999),new Human("mam2","gag2",1999)),
                new Family(new Human("pap3","gag3",1999),new Human("mam3","gag3",1999)),
                new Family(new Human("pap4","gag4",1999),new Human("mam4","gag4",1999))
        ));
        FamilyDao familyDao = new CollectionFamilyDao(familyList);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.getAllFamilies().forEach(el -> el.toString());
        System.out.println("----------------------------------");
        familyController.displayAllFamilies();
        System.out.println("----------------------------------");
        System.out.println(familyController.count());
        System.out.println("----------------------------------");
        familyController.getFamilyById(3).toString();//pap4 and mam4
        System.out.println("----------------------------------");
        familyController.adoptChild(familyList.get(2),new Human("Oleg","gag3",2009));
        familyController.getFamilyById(2).toString();
        System.out.println("----------------------------------");
        familyController.bornChild(familyList.get(0),"Igor","Inna");
        familyController.getFamilyById(0).toString();
        System.out.println("----------------------------------");
        familyController.getFamiliesBiggerThen(3).forEach(el -> el.toString());
        System.out.println("----------------------------------");
        familyController.getFamiliesBiggerThen(2).forEach(el -> el.toString());
        System.out.println("----------------------------------");
        System.out.println("Number of families:" + familyList.size());
        System.out.println("|||||||||||||||||");
        System.out.println("Number of families with 3 members: " + familyController.countFamiliesWithMemberNumber(3));
        System.out.println("----------------------------------");
        System.out.println("Last family in list before applying method :");
        familyList.get(familyList.size() -1).toString();
        System.out.println("Last family in list after applying method :");
        familyController.createNewFamily(new Human("pap5","gag5",1993),new Human("mam5","gag5",1991));
        familyList.get(familyList.size() -1).toString();
        System.out.println("----------------------------------");
        familyController.addPet(1,new Dog("guk2",3,(byte) 100));
        familyController.addPet(1,new Dog("guk48",10,(byte) 300));
        familyController.getFamilyById(1).toString();
        System.out.println("----------------------------------");
        Arrays.toString( familyController.getPets(1).toArray());
        //нету животных ->  null
        if(familyController.getPets(0).size() == 0){
            System.out.println("null");//+
        }else
        Arrays.toString( familyController.getPets(0).toArray());// count -> 0
        System.out.println("----------------------------------");
        familyList.get(2).toString();//  было два родителя с индексом 3 + ребенок
        familyController.deleteFamilyByIndex(2);//удалили их
        familyList.get(2).toString();// все здинулось на 1, теперь два родителя со след интексом в этой ячейки
        System.out.println("----------------------------------");
        for (int i = 0; i < familyList.size(); i++) {
            familyList.get(i).getChildren().add(new Human("child1", "child1", 2018));
            familyList.get(i).getChildren().add(new Human("child2", "child2",2010 ));
        }
        familyController.displayAllFamilies();
        System.out.println("and");// появились дети добавлены выше 18го года и 10го
        int age = 5;

        familyController.deleteAllChildrenOlderThen(age);//удаляем детей старше 5ти лет
        familyController.displayAllFamilies();//остались дети от 18го года, которым менше 5ти лет включительно

    }
}
